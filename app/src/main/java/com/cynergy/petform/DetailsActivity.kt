package com.cynergy.petform

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        val bundle = intent.extras

        bundle?.let {
            var name = it.getString("key_name")?:""
            tvName.text = "Nombre de la mascota: $name"
            var age = it.getString("key_age")?:""
            tvAge.text = "Edad: $age"
            var pet = it.getString("key_pet")?:""
            when(pet){
                "Perro" -> ivPet.setImageDrawable(getDrawable(R.drawable.dog))
                "Gato" -> ivPet.setImageDrawable(getDrawable(R.drawable.cat))
                "Conejo" -> ivPet.setImageDrawable(getDrawable(R.drawable.rabbit))
            }

            var vacuns = it.getString("key_vacuns")?:""
            tvVacuns.text = "Vacunas:\n$vacuns"
        }

    }
}