package com.cynergy.petform

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.view.get
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnSend.setOnClickListener {

            var name = etName.text.toString()
            var age = etAge.text.toString().toIntOrNull()

            if (name.isNullOrEmpty()){
                Toast.makeText(this,"Ingrese el nombre de su mascota",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (age == null){
                Toast.makeText(this,"Ingrese la edad de su mascota",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (rgPets.checkedRadioButtonId == -1){
                Toast.makeText(this,"Seleccione una mascota",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            var pet = when(rgPets.checkedRadioButtonId){
                    R.id.rbDog -> "Perro"
                    R.id.rbCat -> "Gato"
                    R.id.rbRabitt -> "Conejo"
                else -> ""
            }

            var vacuns = ""
            if(chkParvovirus.isChecked == true) vacuns += "Parvovirus\n"
            if(chkLeptospirosis.isChecked == true) vacuns += "Leptospirosis\n"
            if(chkTraqueitis.isChecked == true) vacuns += "Traqueitis\n"
            if(chkLeishmania.isChecked == true) vacuns += "Leishmania\n"
            if(chkRabia.isChecked == true) vacuns += "Rabia\n"

            var a = "a";

            var bundle = Bundle().apply {
                putString("key_name",name)
                putString("key_age",age.toString())
                putString("key_pet",pet)
                putString("key_vacuns",vacuns)
            }

            val intent = Intent(this,DetailsActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)

        }

    }
}